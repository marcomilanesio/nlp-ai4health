# nlp-ai4health

Argumentative Analysis of Clinical Trials for Evidence Based Medicine

# NOTE: all notebooks will be available on Friday afternoon.

## Getting started

### Colab or Jupyter

```
pip install nltk
pip install tensorflow keras
pip install scikit-learn
pip install biobert-embedding

git clone https://gitlab.com/tomaye/abstrct.git
wget http://nlp.stanford.edu/data/glove.6B.zip

nltk.download('book')
nltk.download('omw-1.4')   # may be necessary
```

If you use Google Colab, create a folder on your Google Drive folder (say: "share").
Then, on the Colab notebook:

```
from google.colab import drive
drive.mount('/content/gdrive')  # need authentication
```

At this point you can

```
% cd gdrive/MyDrive/share  # notice the "%" sign at the beginning!!!
```

Now your folder is your current working directory for this notebook.
